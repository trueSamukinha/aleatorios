<?php

$turma = array(
    array("nome" => "Camila Machado", "b1" => 18, "b2" => 20, "b3" => 15, "b4" => 15),
    array("nome" => "José Augusto", "b1" => 11, "b2" => 20, "b3" => 11, "b4" => 13),
    array("nome" => "Bárbara Lima", "b1" => 15, "b2" => 10, "b3" => 10, "b4" => 12),
    array("nome" => "Samukinha", "b1" => 20, "b2" => 30, "b3" => 20, "b4" => 30),
    array("nome" => "Ayanokoji Kiyotaka", "b1" => 12, "b2" => 18, "b3" => 12, "b4" => 18),
);

echo "<table border='1'>";
echo "<tr><th>Nome</th><th>B1</th><th>B2</th><th>B3</th><th>B4</th></tr>";
foreach ($turma as $aluno) {
    echo "<tr>";
    foreach ($aluno as $key => $value) {
        echo "<td>$value</td>";
    }
    echo "</tr>";
}
echo "</table>";


// $turma2 = array();

// $turma2[2020001] = $turma[0];
// $turma2[2020002] = $turma[1];
// $turma2[2020003] = $turma[2];
// $turma2[2020004] = $turma[3];
// $turma2[2020005] = $turma[4];

// echo '<pre>';
// print_r($turma2);
// echo '</pre>';

?>