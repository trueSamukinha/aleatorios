<?php 
    $funcionarios = array(
        array("nome" => "Alex", "idade" => 21, "salario" => 1285.27, "ativo" => true),
        array("nome" => "Emerson", "idade" => 35, "salario" => 3885.27, "ativo" => false),
        array("nome" => "Osvaldo", "idade" => 54, "salario" => 5285.27, "ativo" => true),
        array("nome" => "Mariana", "idade" => 38, "salario" => 4898.25, "ativo" => true),
    );
    
    echo "<pre>";
    print_r($funcionarios);
    echo "</pre>";

    $bonificacao = 0.10;
 
    echo "<br>Manipulação sem alterar os dados do array funcionários<br>";
    foreach($funcionarios as $func){

        if($func["ativo"]){
            $func["salario"] += $func["salario"] * $bonificacao;    
                echo "Funcionario: {$func['nome']} - {$func['salario']}<br>";
            } else {
                echo "Funcionario: {$func['nome']} - INATIVO<br>";
            }
    }
        

    foreach ($funcionarios as $i => $func){
        
        if($func['ativo']){
            $funcionarios[$i]['salario'] += $func['salario']*$bonificacao;
        }

    }

    echo "<br>Atualização do array funcionários";
    echo "<pre>";
    print_r($funcionarios);
    echo "</pre>";

    
?>