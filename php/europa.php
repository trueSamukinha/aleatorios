<?php

$europa = array ("Itália" => "Roma", 
					"Luxemburgo" => "Luxemburgo", 
					"Bélgica" => "Bruxelas", 
					"Dinamarca" => "Copenhague", 
					"Finlândia" => "Helsinki", 
					"França" => "Paris",
					"Eslováquia" => "Bratislava",
					"Eslovênia" => "Liubliana",
					"Alemanha" => "Berlim",
					"Grécia" => "Atenas",
					"Irlanda" => "Dublin",
					"Países Baixos" => "Amsterdã",
					"Portugal" => "Lisboa",
					"Espanha" => "Madrid",
					"Suecia" => "Estocolmo",
					"Reino Unido" => "Londres",
					"Chipre" => "Nicósia",
					"Lituânia" => "Vilnius",
					"República Tcheca" => "Praga",
					"Estônia" => "Tallin",
					"Hungria" => "Budapeste",
					"Letônia" => "Riga",
					"Malta" => "Valeta",
					"Áustria" => "Viena",
					"Polónia" => "Varsóvia");

    asort($europa);
    echo "<br>Após asort";
    echo "<pre>";
    
    foreach($europa as $pais => $capital){
		echo "<br>A Capital do pais ".$pais." eh ".$capital;
	}

?>